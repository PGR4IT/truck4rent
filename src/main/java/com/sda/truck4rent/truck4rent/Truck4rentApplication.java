package com.sda.truck4rent.truck4rent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Truck4rentApplication {

    public static void main(String[] args) {
        SpringApplication.run(Truck4rentApplication.class, args);
    }

}
